package tptema3.ui;

import tptema3.model.Customer;
import tptema3.services.CustomerService;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;


public class RegisterWindow {
	
	JFrame regFrame;													//RegisterWindow - frame		//database user password
	private JLabel lblUsername, lblPassword;							//username/password - labels
	private JTextField username;										// username Input - textfield
	private JPasswordField password;									// password Input - textield
	private JButton regButton;											// confirm register button

	
	public RegisterWindow() {
		initialize();
	}
	
	private void initialize () {
		regFrame = new JFrame();
		regFrame.setBounds(100, 100, 800, 600);
		regFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		regFrame.setLocationRelativeTo(null);
		regFrame.setResizable(false);
		
		
		JPanel panel = new JPanel();
		panel.setLayout(null);
		panel.setBounds(300, 70, 290, 120);
		regFrame.getContentPane().add(panel);
	
		username = new JTextField ();
		username.setBounds(300, 150, 150, 20);
		panel.add(username);
		username.setColumns(10);
		
		password = new JPasswordField ();
		password.setBounds(300, 200, 150, 20);
		panel.add(password);
		password.setColumns(10);
		password.setEchoChar('*');
		
		lblUsername = new JLabel ("User: ");
		lblUsername.setForeground(Color.RED);
		lblUsername.setBounds(230,150,150,20);
		panel.add(lblUsername);
		
		lblPassword = new JLabel("Pasword: ");
		lblPassword.setForeground(Color.RED);
		lblPassword.setBounds(230,200,150,20);
		panel.add(lblPassword);
		
		
		regButton = new JButton("Confirm Register");
		regButton.setBounds(300,300,150,20);
		regButton.setForeground(Color.BLACK);
		panel.add(regButton);
		
		regButton.addActionListener(new ActionListener () {
			@SuppressWarnings("deprecation")
			@Override
			public void actionPerformed(ActionEvent arg0) {
				// TODO Auto-generated method stub
				String uName = null, uPass = null;

				//Password & Username input
				uPass = password.getText();
				uName = username.getText();
				
				int valid=0, valid1=0, valid2=0, valid3=0;
				
				//Username entered
				if (uName.length() > 0) {
					valid = 1;
					//Password entered
					if (uPass.length() > 0) {
						valid1 = 1;
						//Password longer than 6 characters
						if (uPass.length() < 6) {
							valid2 = 0;
							JOptionPane.showMessageDialog(null, "Password at least 6 characters !", "Invalid login!",JOptionPane.INFORMATION_MESSAGE);
							password.setText(null);
						}
						else {
							valid2 = 1;
							//Password contains numbers
							if (!uPass.matches(".*[0-9].*")) {
								valid3=0;
								JOptionPane.showMessageDialog(null, "Password must contain at least one number !", "Invalid login!",JOptionPane.INFORMATION_MESSAGE);
								password.setText(null);
							}
							else {
								valid3=1;
							}
						}
					}
					else {
						JOptionPane.showMessageDialog(null, "Enter password !", "Invalid login!",JOptionPane.INFORMATION_MESSAGE);
					}
				}
				else {
					JOptionPane.showMessageDialog(null, "Enter username !", "Invalid login!",JOptionPane.INFORMATION_MESSAGE);
				}
				

				

				

				
				if (valid==1 && valid1==1 && valid2 == 1 && valid3==1) {
					int status = updateDatabase();

					if (status == -3) {
						JOptionPane.showMessageDialog(null, "Account with same user already exists !", "Account already exists !",JOptionPane.ERROR_MESSAGE);
					}

					LoginGUI window = new LoginGUI();
					window.frame.setVisible(true);
					window.frame.setResizable(false);
					window.frame.setLocationRelativeTo(null);
					
					regFrame.setVisible(false);
				}
			}
				
		});
	}
	private int updateDatabase ( ){
		CustomerService customerService= new CustomerService();
		Customer customer = new Customer();
		customer.setCustomerName(username.getText());
		customer.setCustomerPassword(password.getText());
		return customerService.createCustomer(customer);
	}

	public JFrame getRegFrame() {
		return regFrame;
	}

	public void setRegFrame(JFrame regFrame) {
		this.regFrame = regFrame;
	}
}
