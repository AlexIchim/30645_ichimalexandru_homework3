package tptema3.ui;

import tptema3.model.Customer;
import tptema3.services.CustomerService;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.*;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;


public class LoginGUI {
	
	public JFrame frame;		//connection session with a specific database
	private JLabel lblPassword, lblUsername;					// password/username - label
	private JTextField username;								// Username Input - field
	private JPasswordField password;							// Password Input - field
	private JButton loginButton, regButton;						// login / register - button

	private CustomerService customerService;
	private RegisterWindow registerWindow;
	private UserWindow userWindow;
	
	public static void main (String[] args) {
		EventQueue.invokeLater(new Runnable(){

			@Override
			public void run() {
				// TODO Auto-generated method stub
				try {
					LoginGUI view = new LoginGUI();
					view.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	

	
	public LoginGUI() {
		customerService = new CustomerService();
		initialize();			//LoginGUI components initialize
	}


	private void initialize () {
		
		frame = new JFrame();
		frame.setBounds(100, 100, 800, 600);
		frame.setTitle("Order Management");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setLocationRelativeTo(null);
		frame.setResizable(false);
	
	
		JPanel panel = new JPanel();
		panel.setOpaque(false);
		panel.setLayout(null);
		panel.setBounds(300, 70, 290, 120);
		frame.getContentPane().add(panel);
	
	
		username = new JTextField ();
		username.setBounds(300, 150, 150, 20);
		panel.add(username);
		username.setColumns(10);
		
		password = new JPasswordField ();
		password.setBounds(300, 200, 150, 20);
		panel.add(password);
		password.setColumns(10);
		
		lblUsername = new JLabel ("User: ");
		lblUsername.setForeground(Color.RED);
		lblUsername.setBounds(230,150,150,20);
		panel.add(lblUsername);
		
		lblPassword = new JLabel("Password: ");
		lblPassword.setForeground(Color.RED);
		lblPassword.setBounds(230,200,150,20);
		panel.add(lblPassword);

		loginButton = new JButton("Login");
		loginButton.setBounds (300, 250, 150, 20);
		panel.add(loginButton);
		
		loginButton.addActionListener( new ActionListener() {
				public void actionPerformed (ActionEvent arg0) {
					int valid=0, valid1=0;
					String pword = null, user = null;
					try {
						System.out.println("Action listenr");
						Customer customer = new Customer();
						customer.setCustomerName(username.getText());
						customer.setCustomerPassword(String.valueOf(password.getPassword()));

						if (!customerService.selectCustomer(customer)) {
							JOptionPane.showMessageDialog(null, "Invalid Login Info");
							username.setText("");
							password.setText("");
						} else  {
								userWindow = new UserWindow();
								userWindow.getLoginFrame().setVisible(true);
								username.setText(null);
								password.setText(null);
								frame.setVisible(false);
						}
					}	catch(Exception ex) {
							ex.printStackTrace();
					}
				}	
		});
		regButton = new JButton ("Register");
		regButton.setBounds(300,300,150,20);
		panel.add(regButton);
		regButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				try {
					registerWindow = new RegisterWindow();
					registerWindow.getRegFrame().setVisible(true);
					username.setText(null);
					password.setText(null);
					frame.setVisible(false);
				}	catch (Exception ex) {
						ex.printStackTrace();
				}
			}
		});
	}
}
