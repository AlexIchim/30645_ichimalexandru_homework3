package tptema3.ui;

import tptema3.dao.ProductDao;
import tptema3.model.*;
import tptema3.services.CustomerService;
import tptema3.services.OrderService;
import tptema3.services.ProductService;

import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.*;

import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;

public class UserWindow {
	
	
	/**
	 * Create the app
	 */
	
	public Warehouse wHouse;								// Depozitul de produse
	public OPDept oDept;									// TreeSet - de comenzi efectuate de clienti
	private JList<Order> orderList;							//	Lista de comenzi
	private DefaultTableModel modelWhouse, modelOrder;		//	Tabel pentru depozit si continut comanda
	private DefaultListModel<Order> listModel;				//	Lista pentru comenzi
	
	private int idNr=0;
	JFrame loginFrame;
	private JTextField prodName, prodQuantity;
	private JLabel lblProdName, lblProdQuantity, lblOrders, lblOrderInfo, lblWarehouse;
	private JButton addProdButton, addOrderButton, removeOrderButton, deleteButton, underStockButton, overStockButton;
	private JTable tblWarehouse, tblOrderInfo;
	private JScrollPane ordersScrl, ordersInfoScrl, whouseScrl;
	
	private JMenuBar menuBar;
	private JMenu menu;
	private JMenuItem menuItem, menuItem2;

	private CustomerService customerService;
	private OrderService orderService;
	private ProductService productService;
	
	
	public UserWindow() {
		wHouse = new Warehouse();
		oDept = new OPDept();
		listModel = new DefaultListModel<>();
		modelWhouse = new DefaultTableModel();
		modelOrder = new DefaultTableModel();
		initialize();
		fillData();
		customerService = new CustomerService();
		orderService = new OrderService();
		productService = new ProductService();
	}

	private void initialize () {
		loginFrame = new JFrame();
		loginFrame.setBounds (100, 100, 450, 670);
		loginFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		loginFrame.setTitle("Order Management");
		loginFrame.setLocationRelativeTo(null);
		loginFrame.getContentPane().setLayout(null);
	
		//Create the menu Bar
		menuBar = new JMenuBar();
		menuBar.setBounds(0, 0, 441, 21);
		loginFrame.getContentPane().add(menuBar);
		
		//
		menu = new JMenu("Menu");
		menu.setMnemonic(KeyEvent.VK_M);
		menuBar.add(menu);
		
		
		//About meniu
			
			menuItem = new JMenuItem("Help");
			menuItem.addMouseListener( new MouseAdapter() {
				
				@Override
				public void mousePressed (MouseEvent e) {
					
					JOptionPane.showMessageDialog(null,"1.Pentru adaugarea unui produs: - introduceti numele si cantitatea din produsul dorit.\n2.La crearea unei comenzi introduceti, numele clientului, produsele separate prin spatiu si cantiatea din fiecare produs separata prin spatiu, dupa care apasati OK.\n 3. Butonul de Release Order, va elibera comanda selectata si va actualiza baza de date cu datele noi.\n3.Butonul Remove order este pentru stergerea unei comenzi, fara sa afecteze baza de date.\n4.Understock reprezinta produsele avand cantitatea < 100, iar Overstock produsele avand cantitatea mai mare de 100.");
				}
			});
			menu.add(menuItem);
			
		
		//Quit app menu
			
			menuItem2 = new JMenuItem("Quit");
			menuItem2.addMouseListener(new MouseAdapter () {
				@Override
				public void  mousePressed(MouseEvent e) {
					System.exit(0);
				}
			});
			menu.add(menuItem2);
		
		
		
		

		//WareHouse content - display
		whouseScrl = new JScrollPane();
		whouseScrl.setBounds(10, 50, 170, 160);
		tblWarehouse = new JTable (modelWhouse);
		whouseScrl.setViewportView(tblWarehouse);
		loginFrame.getContentPane().add(whouseScrl);
		
	
		lblWarehouse = new JLabel ("Warehouse");
		lblWarehouse.setBounds(40,30,95,25);
		loginFrame.getContentPane().add(lblWarehouse);
	
		prodName = new JTextField ();
		prodName.setBounds(310, 70, 95, 20);
		prodName.setColumns(10);
		loginFrame.getContentPane().add(prodName);
		
		
		lblProdName = new JLabel ("Product Name");
		lblProdName.setBounds(185,65,115,25);
		loginFrame.getContentPane().add(lblProdName);
		
		prodQuantity = new JTextField();
		prodQuantity.setBounds(310,100,95,20);
		prodQuantity.setColumns(10);
		loginFrame.getContentPane().add(prodQuantity);
		
		lblProdQuantity = new JLabel ("Quantity");
		lblProdQuantity.setBounds(185, 100, 90, 25);
		loginFrame.getContentPane().add(lblProdQuantity);
			
	
		
		//Add a product to the database (specify quantity and name)
		addProdButton = new JButton ("Add Product");
		addProdButton.setBounds(230, 140, 120, 25);
		loginFrame.getContentPane().add(addProdButton);


		addProdButton.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					// TODO Auto-generated method stub
					String name = null;
					int quant1 = 0;
					if ( prodQuantity.getText().length()> 0 && prodName.getText().length() > 0) {
						name = prodName.getText();
						quant1 = Integer.parseInt(prodQuantity.getText());
						Product pr = new Product(name, quant1);
						if (!wHouse.addProduct(pr)) {
							int quant2 =  Product.getExportQuantity() + quant1;
							wHouse.removeProduct(pr);
							Product newProd = new Product(name, quant2);
							wHouse.addProduct(newProd);
							productService.updateProduct(newProd);
						} else  {
							productService.addProduct(pr);
						}
						
						//Initialize table rows to 0 then fill table content with products
						modelWhouse.setRowCount(0);
						for (Product products : wHouse.getTreeSet()) {
							modelWhouse.addRow(new Object[] {products.getProductName(), products.getProductQuantity()});
						}


						JOptionPane.showMessageDialog(loginFrame.getContentPane(), "Product added!" + "\n" + prodName.getText() + " " + prodQuantity.getText());
						prodName.setText("");
						prodQuantity.setText("");
					} else {
						JOptionPane.showMessageDialog(loginFrame.getContentPane(), "Please fill both fields !");
					}
				}
		});
	
		
		
	lblOrders = new JLabel ("Orders");
	lblOrders.setBounds(40,220,100,25);
	loginFrame.getContentPane().add(lblOrders);
		
		
	//Order List display model
	ordersScrl = new JScrollPane();
	ordersScrl.setBounds(10, 250, 170, 160);
	loginFrame.getContentPane().add(ordersScrl);
	orderList = new JList<>(listModel);
	ordersScrl.setViewportView(orderList);

	
	//Single selection list
	orderList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
	
	
	orderList.addListSelectionListener (new ListSelectionListener () { 
		@Override
		public void valueChanged(ListSelectionEvent e) {
			// TODO Auto-generated method stub
			if (e.getValueIsAdjusting()) {
				Order selectedOrder = orderList.getSelectedValue();
				
				//Initialize list to 0 then add the order products to the table
				modelOrder.setRowCount(0);
				for (Product products : selectedOrder.getProducts()) {
						modelOrder.addRow(new Object[]{products.getProductName(), products.getProductQuantity()});
				}
			}
		}
	});
		
	addOrderButton = new JButton("Add order");
	addOrderButton.setBounds(220,290,150,25);
	loginFrame.getContentPane().add(addOrderButton);
	
	addOrderButton.addActionListener(new ActionListener() {
		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			JTextField uName = new JTextField (), pName = new JTextField(), quantity = new JTextField();

			List<Customer> customrsList = customerService.selectCustomers();
			String[] custList = new String[customrsList.size()];
			int i =0;
			for (Customer cust: customrsList
				 ) {
				custList[i] = cust.getCustomerName();
				i++;
			}
			JComboBox userList = new JComboBox(custList);
			Object[] msg = { "Client:" ,userList,
							  "Product: ", pName,
							  "Quantity: ", quantity,
							  "Thank you for your order !"};
			
			int opt = JOptionPane.showConfirmDialog(null, msg, "Add Order", JOptionPane.OK_CANCEL_OPTION);


			if (opt == JOptionPane.OK_OPTION) {
				if (userList.getSelectedItem().toString().length() <= 0) {
					JOptionPane.showMessageDialog(null, "Enter Client name !");
				}
				else if (pName.getText().length() <= 0) {
							JOptionPane.showMessageDialog(null, "Enter Products separated by a blank !");
				}
				else if (quantity.getText().length() <= 0) {
							JOptionPane.showMessageDialog(null, "Enter Products quantity separated by a blank !");
				}
				else
				{
							Customer customer = new Customer ();
							customer.setCustomerName(userList.getSelectedItem().toString());
							String str1 =  pName.getText();
							String str2 = quantity.getText();
				
				
							StringTokenizer tokens1 = new StringTokenizer(str1, " ");
							StringTokenizer tokens2 = new StringTokenizer(str2, " ");
				
							List<Product> productsL = new ArrayList<Product>();
							int ok=0;
							while (tokens1.hasMoreTokens() && tokens2.hasMoreTokens()) {
								Product pr = new Product (tokens1.nextToken(), Integer.parseInt(tokens2.nextToken()));
								if (wHouse.addProduct(pr)) {
									wHouse.removeProduct(pr);
									JOptionPane.showMessageDialog(null, "Product does not exists !", "Something went wrong !", JOptionPane.INFORMATION_MESSAGE);
									ok=1;
								}
								else {
									productsL.add(pr);
								}	
							}
				
							//Daca input-ul in fereastra este valid
							if (ok==0) {
								idNr++;
								Order ord = new Order(customer, productsL);
								int id = orderService.createOrder(ord);
								ord.setOrderId(id);
					
								//daca mai exista o comanda de la acelasi client adaugam alta comanda
							/*	if (!oDept.addOrder(ord)) {
									oDept.removeOrder(ord);
									oDept.addOrder(ord);
								}*/
								oDept.addOrder(id, ord);

								listModel.clear();
								for (Order order : oDept.getTreeMap().values()) {
									listModel.addElement(order);
								}
							}
						}
				}	
		}
		
	});
	
	ordersInfoScrl = new JScrollPane();
	ordersInfoScrl.setBounds(10, 450, 165, 160);
	loginFrame.getContentPane().add(ordersInfoScrl);
	tblOrderInfo = new JTable(modelOrder);
	ordersInfoScrl.setViewportView(tblOrderInfo);
	
	lblOrderInfo = new JLabel("Order info");
	lblOrderInfo.setBounds(60,430,150,25);
	loginFrame.getContentPane().add(lblOrderInfo);
	
	removeOrderButton = new JButton ("Release order");
	removeOrderButton.setBounds(220, 320, 150, 25);
	loginFrame.getContentPane().add(removeOrderButton);
	removeOrderButton.addActionListener(new ActionListener () {

		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			int valid = 1;
			Order selectedOrder = orderList.getSelectedValue();
			if (selectedOrder != null) {
				for (Product products : selectedOrder.getProducts()) {
					if (!wHouse.addProduct(products)) {
						int quant = Product.getExportQuantity();
						String name = products.getProductName();
						quant -= products.getProductQuantity();
						if (quant >= 0) {
							wHouse.removeProduct(products);
							Product pr = new Product(name, quant);
							wHouse.addProduct(pr);
						} else {
							valid = 0;
							JOptionPane.showMessageDialog(null, "Not enough quantity", "Something went wrong!", JOptionPane.INFORMATION_MESSAGE);
						}
					}
				}

				if (valid == 1 && selectedOrder != null) {


					oDept.removeOrder(selectedOrder);

					listModel.clear();
					for (Order orde : oDept.getTreeMap().values()) {
						listModel.addElement(orde);
					}

					//Actualizare Whouse table and OrderInfo table
					modelOrder.setRowCount(0);
					modelWhouse.setRowCount(0);

					for (Product products : wHouse.getTreeSet()) {
						modelWhouse.addRow(new Object[]{products.getProductName(), products.getProductQuantity()});
					}

					releaseOrder(selectedOrder);
					JOptionPane.showMessageDialog(null, "Order done !");
				}
			}
			else {
					JOptionPane.showMessageDialog(null, "Please select and order", "Something went wrong!",JOptionPane.INFORMATION_MESSAGE );
			}
		}
	});
	
	
	deleteButton = new JButton ("Remove order");
	deleteButton.setBounds(220, 350, 150, 25);
	loginFrame.getContentPane().add(deleteButton);
	
	deleteButton.addActionListener(new ActionListener() {

		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			Order selOrder = orderList.getSelectedValue();

			if (selOrder != null) {
				oDept.removeOrder(selOrder);

				listModel.clear();
				Iterator it = oDept.getTreeMap().entrySet().iterator();
				while (it.hasNext()) {
					Map.Entry pair = (Map.Entry)it.next();
					listModel.addElement((Order) pair.getValue());
					it.remove(); // avoids a ConcurrentModificationException
				}

				for (Order orde : oDept.getTreeMap().values()) {
					listModel.addElement(orde);
				}

				//Init order info
				modelOrder.setRowCount(0);
				deleteOrder(selOrder);
			} else {
				JOptionPane.showMessageDialog(null, "Please select and order", "Something went wrong!", JOptionPane.INFORMATION_MESSAGE);
			}
		}
	});
	
	
	//Check for Products understocked  (less than 100 quantity)
	underStockButton = new JButton ("UnderStock");
	underStockButton.setBounds(220,450,150,25);
	loginFrame.getContentPane().add(underStockButton);
	
	underStockButton.addActionListener(new ActionListener() {

		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			String str = "Products Understocked:\n";
			for (Product products : wHouse.getTreeSet()) {
				if (products.getProductQuantity() <= 100) {
					str+=products.getProductName() +"\n";
				}
			}
			JOptionPane.showMessageDialog(null, str, "Understocked", JOptionPane.INFORMATION_MESSAGE);
		}
		
	});
	
	
	//Check for overStocked products (more than 100)
	overStockButton = new JButton ("OverStock");
	overStockButton.setBounds(220,480,150,25);
	loginFrame.getContentPane().add(overStockButton);
	
	overStockButton.addActionListener(new ActionListener() {

		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			String str = "Products Overstocked:\n";
			for (Product products : wHouse.getTreeSet()) {
				if (products.getProductQuantity() > 100) {
					str+=products.getProductName() +"\n";
				}
			}
			JOptionPane.showMessageDialog(null, str, "Overstocked", JOptionPane.INFORMATION_MESSAGE);
		}
		
	});
	
	
	}
	
	
	//Filling the interaface with data from the database
	private void fillData () {

		ProductDao productDao = new ProductDao();
		List<Product> productList = productDao.getProducts();
	
		//Whouse table model
		modelWhouse.addColumn("Name");
		modelWhouse.addColumn("Quantity");
		
		
		//Order table model
		modelOrder.addColumn("Name");
		modelOrder.addColumn("Quantity");

		for (Product pr: productList
			 ) {
			wHouse.addProduct(pr);
		}

		for (Product products : wHouse.getTreeSet()) {
			modelWhouse.addRow(new Object[] { products.getProductName(), products.getProductQuantity()});
		}
	}

	private void releaseOrder(Order order) {
		CreateReport.createReport(order);
		for (Product prod : order.getProducts()
			 ) {
			Product product = productService.getProductById(prod.getProductId());
			product.setProductQuantity(product.getProductQuantity() - prod.getProductQuantity());
			productService.updateProduct(prod);
		}
		orderService.releaseOrder(order);
	}

	private void deleteOrder(Order order) {
		orderService.deleteOrder(order);
	}
		
		
		
	public static void main (String[] args) {
		EventQueue.invokeLater(new Runnable(){

			@Override
			public void run() {
				// TODO Auto-generated method stub
				try {
					UserWindow view = new UserWindow();
					view.loginFrame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}


	public JFrame getLoginFrame() {
		return loginFrame;
	}


}
