package tptema3.services;

import tptema3.dao.ProductDao;
import tptema3.model.Product;

import java.util.List;

/**
 * Created by Alex Ichim on 20.04.2017.
 */
public class ProductService {

    private ProductDao productDao;

    public ProductService() {
        productDao = new ProductDao();
    }

    public void addProduct(Product product) {
        productDao.addProduct(product);
    }


    public void updateProduct(Product product) {
       productDao.updateProduct(product);
    }

    public Product getProductById(int productId) {
        return productDao.getProductById(productId);
    }


    public Product getProductByName(String productName) {
        return productDao.getProductByName(productName);
    }


    public List<Product> getProducts() {
        return productDao.getProducts();
    }
}
