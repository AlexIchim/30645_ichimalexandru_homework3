package tptema3.services;

import com.mysql.jdbc.exceptions.MySQLIntegrityConstraintViolationException;
import tptema3.dao.CustomerDao;
import tptema3.model.Customer;

import java.util.List;

/**
 * Created by Alex Ichim on 20.04.2017.
 */
public class CustomerService {

    private CustomerDao customerDao;

    public CustomerService() {
        customerDao = new CustomerDao();
    }

    public int createCustomer(Customer customer) {
        if (customerDao.selectCustomerByUsername(customer))
            return -3;
        else
            return customerDao.createCustomer(customer);
    }

    public void updateCustomer(Customer customer) {
        customerDao.updateCustomer(customer);
    }

    public boolean selectCustomer(Customer customer) {
        return customerDao.selectCustomer(customer);
    }

    public boolean selectCustomerByUsername(Customer customer) { return  customerDao.selectCustomer(customer); }

    public List<Customer> selectCustomers() {
        return customerDao.selectCustomers();
    }
}
