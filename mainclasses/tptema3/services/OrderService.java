package tptema3.services;

import tptema3.dao.OrderDao;
import tptema3.model.Order;

import java.util.List;

/**
 * Created by Alex Ichim on 20.04.2017.
 */
public class OrderService {
    private OrderDao orderDao;
    private ProductOrderService productOrderService;

    public OrderService() {
        orderDao = new OrderDao();
        productOrderService = new ProductOrderService();
    }

    public int createOrder(Order order) {
        return orderDao.createOrder(order);
    }

    public List<Order> getOrders() {
        return orderDao.getOrders();
    }

    public void releaseOrder(Order order) {
        orderDao.releaseOrder(order);
    }

    public void deleteOrder(Order order) {
        productOrderService.deleteProductOrder(order);
        orderDao.deleteOrder(order);
    }
}
