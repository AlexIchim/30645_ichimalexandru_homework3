package tptema3.services;

import com.sun.org.apache.xpath.internal.operations.Or;
import tptema3.dao.ProductOrderDao;
import tptema3.model.Order;
import tptema3.model.Product;

import java.util.List;

/**
 * Created by Alex Ichim on 20.04.2017.
 */
public class ProductOrderService {

    private ProductOrderDao productOrderDao;

    public ProductOrderService() {
        productOrderDao = new ProductOrderDao();
    }

    public void create(Product product, int orderId) {
        productOrderDao.create(product, orderId);
    }

    public List<Product> getProductsForOrder(int orderId) {
        return productOrderDao.getProductsForOrder(orderId);
    }

    public void deleteProductOrder( Order order) {
         productOrderDao.deleteProductOrder(order);
    }
}
