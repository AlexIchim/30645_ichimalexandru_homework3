package tptema3.model;

public class Product implements Comparable<Product> {

	/**
	 * @param productName - Numele produsului
	 * @param productQuantity - Cantitatea din produsul respectiv.
	 */

	private int productId;
	private String productName;
	private int productQuantity;

	static int exportQuantity;
	
	public Product () {
	}
	
	public Product (String pName, int pQuantity) {
			this.productName = pName;
			this.productQuantity = pQuantity;
	}
	
	public String getProductName () {
		return this.productName;
	}
	
	public int getProductQuantity () {
		return this.productQuantity;
	}
	
	public void setProductQuantity (int pQuantity) {
		this.productQuantity = pQuantity;
	}
	
	public void setProductName (String pName) {
		this.productName = pName;
	}

	public int getProductId() {
		return productId;
	}

	public void setProductId(int productId) {
		this.productId = productId;
	}

	@Override
	public String toString () {
		return productName + " : " + productQuantity;
	}
	
	//Compare method for Comparable interface
	@Override
	public int compareTo(Product o) {
		// TODO Auto-generated method stub
		if (this.getProductName().equals(o.getProductName())) {
				exportQuantity = o.getProductQuantity();
				return 0;
		}
		return this.getProductName().compareTo(o.getProductName());
	}

	public static int getExportQuantity() {
		return exportQuantity;
	}
}
