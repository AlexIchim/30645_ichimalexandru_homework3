package tptema3.model;

public class Customer {

	/**
	 * @param customerID - ID-ul de indetificare al clientului
	 * @param customerName - Numele clientului
	 */
	
	private int customerID;
	private String customerName;
	private String customerPassword;
	
	
	//default constructor
	public Customer () {
		
	}
	
	//constructor cu parametrii
	public Customer (int cID, String cName) {
		this.customerID = cID;
		this.customerName = cName;
	}

	public Customer(String customerName, String customerPassword) {
		this.customerName = customerName;
		this.customerPassword = customerPassword;
	}
	
	//getter customerID
	public int getCustomerID () {
		return this.customerID;
	}
	
	//getter customerName
	public String getCustomerName () {
		return this.customerName;
	}
	
	
	//setter customerID
	public void setCustomerID (int cID) {
		this.customerID = cID;
	}
	
	//setter customerName
	public void setCustomerName (String cName) {
		this.customerName = cName;
	}

	public String getCustomerPassword() {
		return customerPassword;
	}

	public void setCustomerPassword(String customerPassword) {
		this.customerPassword = customerPassword;
	}

	//toString method
	@Override
	public String toString ()  {
		return "Name: " + customerName + " ID:" + customerID + "\n";
	}
}
