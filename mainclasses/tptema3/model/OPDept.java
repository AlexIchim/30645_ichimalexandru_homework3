package tptema3.model;

import com.sun.org.apache.xpath.internal.operations.Or;
import sun.reflect.generics.tree.Tree;
import tptema3.model.Order;

import java.util.TreeMap;
import java.util.TreeSet;

public class OPDept {

	/**
	 * @param orders - Comenzile inregistrate de clienti.
	 */
	private TreeMap<Integer, Order> orders;

	//constructor
	public OPDept () {
		this.orders = new TreeMap<>();
	}
	
	//addOrder to TreeSet
	public Order addOrder (int id, Order ord) {
		return this.orders.put(id, ord);
	}
	
	//remove orders from TreeSet
	public void removeOrder (Order ord) {
		this.orders.remove(ord.getOrderId());
	}
	
	//getter TreeSet orders
	public TreeMap<Integer, Order> getTreeMap () {
		return this.orders;
	}
}
