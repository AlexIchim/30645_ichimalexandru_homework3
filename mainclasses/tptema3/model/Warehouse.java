package tptema3.model;

import tptema3.model.Product;

import java.util.TreeSet;

public class Warehouse {
	/**
	 * @param products - List of products stored in warehouse
	 */
	private TreeSet<Product> products;
	
	
	//constructor
	public Warehouse () {
		this.products = new TreeSet<Product>();
	}
	
	//add product to TreeSet
	public boolean addProduct (Product prod) {
		return products.add(prod);
	}
	
	//remove products from TreeSet
	public void removeProduct (Product prod) {
		products.remove(prod);
	}
	
	//getter TreeSet Products
	public TreeSet<Product> getTreeSet () {
		return products;
	}
	
}
