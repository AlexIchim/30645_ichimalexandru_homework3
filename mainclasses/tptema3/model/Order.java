package tptema3.model;

import java.util.List;

public  class Order  implements Comparable<Order> {

	/**
	 * @param customer  - Clientul care detine comanda.
	 * @param products  - Produsele din comanda clientului.
	 */
	private int orderId;
	private Customer customer;
	private List<Product> products;
	
	
	//default Constructor
	public Order () {
		
	}
	
	//constructor cu parametrii
	public Order (Customer c, List<Product> p) {
		this.customer = c;
		this.products = p;
	}
	
	//getter Customer
	public Customer getCustomer () {
		return this.customer;
	}
	
	//getter Products
	public List<Product> getProducts() {
		return this.products;
	}
	
	//setter Customer
	public void setCustomer (Customer c) {
		this.customer = c;
	}
	
	
	//setter Products
	public void setProducts (List<Product> l) {
		this.products = l;
	}
	
	@Override
	public String toString () {
		return customer.getCustomerName() + " with ID:" + customer.getCustomerID() + " has an order.";
	}
	
	
	//CompareTo method from the Comparable interface
	@Override
	public int compareTo(Order o) {
		// TODO Auto-generated method stub
		return this.getCustomer().getCustomerName().compareTo(o.getCustomer().getCustomerName());
	}

	public int getOrderId() {
		return orderId;
	}

	public void setOrderId(int orderId) {
		this.orderId = orderId;
	}
}
