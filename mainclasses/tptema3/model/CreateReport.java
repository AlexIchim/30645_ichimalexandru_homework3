package tptema3.model;

import com.itextpdf.text.*;
import com.itextpdf.text.pdf.PdfWriter;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;

/**
 * Created by Alex Ichim on 20.04.2017.
 */
public class CreateReport {

    public static void main(String[] args) {
        Order order = new Order();
        order.setOrderId(1);
        createReport(order);
    }

    public static void createReport(Order order) {
        Document document = new Document();
        try
        {
            PdfWriter writer = PdfWriter.getInstance(document, new FileOutputStream("Report" + order.getOrderId() +".pdf"));
            document.open();
            document.add(new Paragraph("Order For  Customer:  " + order.getCustomer().getCustomerName()));
            document.add(new Paragraph("Order Products Info " + order.getOrderId()));

            //Add order list
            List orderedList = new List(List.ORDERED);

            document.add(new Paragraph("Product  Quantity"));

            for (Product pr: order.getProducts()
                 ) {
                    orderedList.add(new ListItem(pr.getProductName() + " : " + pr.getProductQuantity()));
            }
            document.add(orderedList);

            document.close();
            writer.close();
        } catch (DocumentException e)
        {
            e.printStackTrace();
        } catch (FileNotFoundException e)
        {
            e.printStackTrace();
        }
    }
}
