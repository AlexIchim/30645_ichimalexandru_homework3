package tptema3.dao;

import com.mysql.jdbc.exceptions.jdbc4.MySQLIntegrityConstraintViolationException;
import tptema3.model.Customer;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Alex Ichim on 08.04.2017.
 */
public class CustomerDao {
    //JDBC driver name and db URL
    static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";
        static final String DB_URL = "jdbc:mysql://localhost:3306/warehouse";


        //Database credentials
        static final String USER = "root";
        static final String PASS = "";

        private Connection conn;

    public CustomerDao(){
            try {
                Class.forName("com.mysql.jdbc.Driver");
                conn = DriverManager.getConnection(DB_URL, USER, PASS);
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            } catch (SQLException e) {
                e.printStackTrace();
            }
    }
    public int createCustomer(Customer customer) {
        try {
            String query = "INSERT INTO users(username, userpassword) VALUES (?, ?)";
            PreparedStatement preparedStatement = conn.prepareStatement(query);
            preparedStatement.setString(1, customer.getCustomerName());
            preparedStatement.setString(2, customer.getCustomerPassword());
            preparedStatement.executeUpdate();
        } catch (MySQLIntegrityConstraintViolationException ex) {
            ex.printStackTrace();
            return -2;
        } catch (SQLException ex) {
            ex.printStackTrace();
            return -1;
        }
        return 0;
    }

    public void updateCustomer(Customer customer) {
        try {
            String query = "UPDATE users SET userpassword= ? WHERE username = ?";
            PreparedStatement preparedStatement = conn.prepareStatement(query);
            preparedStatement.setString(1, customer.getCustomerPassword());
            preparedStatement.setString(2, customer.getCustomerName());
            preparedStatement.executeUpdate();
        } catch (SQLException e ) {
            e.printStackTrace();
        }
    }

    public boolean selectCustomer(Customer customer) {
        String query = "SELECT * FROM users WHERE username = ? AND userpassword = ?";
        try {
            PreparedStatement preparedStatement = conn.prepareStatement(query);
            preparedStatement.setString(1, customer.getCustomerName());
            preparedStatement.setString(2, customer.getCustomerPassword());
            ResultSet rst = preparedStatement.executeQuery();
            if (rst.next()) {
                return true;
            }
        }catch (SQLException ex) {
            ex.printStackTrace();
        }
        return false;
    }

    public boolean selectCustomerByUsername (Customer customer) {
        String query = "SELECT * FROM users WHERE username = ?";
        try {
            PreparedStatement preparedStatement = conn.prepareStatement(query);
            preparedStatement.setString(1, customer.getCustomerName());
            ResultSet rst = preparedStatement.executeQuery();
            if (rst.next()) {
                return true;
            }
        }catch (SQLException ex) {
            ex.printStackTrace();
        }
        return false;
    }


    public List<Customer> selectCustomers() {
        List<Customer> customers = new ArrayList<>();
        String query = "SELECT * FROM users";
        try {
            PreparedStatement preparedStatement = conn.prepareStatement(query);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                Customer customer = new Customer(resultSet.getString(1), resultSet.getString(2));
                customers.add(customer);
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return customers;
    }
}
