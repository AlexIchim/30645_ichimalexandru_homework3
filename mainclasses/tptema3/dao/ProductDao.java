package tptema3.dao;

import tptema3.model.Product;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Alex Ichim on 09.04.2017.
 */
public class ProductDao {

    //JDBC driver name and db URL
    static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";
    static final String DB_URL = "jdbc:mysql://localhost:3306/warehouse";


    //Database credentials
    static final String USER = "root";
    static final String PASS = "";

    private Connection conn;

    public ProductDao(){
        try {
            Class.forName("com.mysql.jdbc.Driver");
            conn = DriverManager.getConnection(DB_URL, USER, PASS);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void addProduct(Product product) {
        String query = "INSERT INTO products (productName, productQuantity) VALUES(?, ?)";
        try {
            PreparedStatement preparedStatement = conn.prepareStatement(query);
            preparedStatement.setString(1, product.getProductName());
            preparedStatement.setInt(2, product.getProductQuantity());
            preparedStatement.executeUpdate();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }

    public void removeProduct(Product product) {

    }

    public void updateProduct(Product product) {
        String query = "UPDATE products SET productQuantity = ? WHERE productName = ?";
        try {
            PreparedStatement preparedStatement = conn.prepareStatement(query);
            preparedStatement.setInt(1, product.getProductQuantity());
            preparedStatement.setString(2, product.getProductName());
            preparedStatement.executeUpdate();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }

    public Product getProductById(int productId) {
        String query = "SELECT * FROM products WHERE productID = ?";
        Product product = new Product();
        try {
            PreparedStatement preparedStatement = conn.prepareStatement(query);
            preparedStatement.setInt(1, productId);
            ResultSet resultSet = preparedStatement.executeQuery();

            if (resultSet.next()) {
                 product.setProductName(resultSet.getString(2));
            }

        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return product;
    }


    public Product getProductByName(String productName) {
        String query = "SELECT * FROM products WHERE productName = ?";
        Product product = new Product();
        try {
            PreparedStatement preparedStatement = conn.prepareStatement(query);
            preparedStatement.setString(1, productName);
            ResultSet resultSet = preparedStatement.executeQuery();

            if (resultSet.next()) {
                product.setProductName(productName);
                product.setProductId(resultSet.getInt(1));
            }

        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return product;
    }


    public List<Product> getProducts() {
        String query = "SELECT * FROM products";
        List<Product> productList = new ArrayList<>();
        try {
            PreparedStatement preparedStatement = conn.prepareStatement(query);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                Product product = new Product(resultSet.getString(2), resultSet.getInt(3));
                productList.add(product);
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return productList;
    }
}
