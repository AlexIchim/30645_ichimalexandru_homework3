package tptema3.dao;

import com.sun.org.apache.regexp.internal.RE;
import tptema3.model.Order;
import tptema3.model.Product;

import javax.xml.transform.Result;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Alex Ichim on 10.04.2017.
 */
public class ProductOrderDao {

    //JDBC driver name and db URL
    static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";
    static final String DB_URL = "jdbc:mysql://localhost:3306/warehouse";


    //Database credentials
    static final String USER = "root";
    static final String PASS = "";

    private Connection conn;
    private ProductDao productDao;

    public ProductOrderDao(){
        productDao = new ProductDao();
        try {
            Class.forName("com.mysql.jdbc.Driver");
            conn = DriverManager.getConnection(DB_URL, USER, PASS);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void create(Product product, int orderId) {
        String query = "INSERT INTO order_has_products(orderID, productID, quantity) VALUES(?,?,?)";
        try {
            PreparedStatement preparedStatement = conn.prepareStatement(query);
            preparedStatement.setInt(1, orderId);

            Product product1 = productDao.getProductByName(product.getProductName());
            preparedStatement.setInt(2, product1.getProductId());

            preparedStatement.setInt(3, product.getProductQuantity());
            preparedStatement.executeUpdate();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }

    public List<Product> getProductsForOrder(int orderId) {
        ProductDao productDao = new ProductDao();

        String query = "SELECT * FROM order_has_products WHERE orderID = ?";
        List<Product> productList = new ArrayList<>();
        try {
            PreparedStatement preparedStatement = conn.prepareStatement(query);
            preparedStatement.setInt(1, orderId);
            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                Product product = productDao.getProductById(resultSet.getInt(2));
                product.setProductQuantity(resultSet.getInt(3));
                productList.add(product);
            }

        } catch (SQLException ex) {
            ex.printStackTrace();
        }

        return productList;
    }

    public void deleteProductOrder(Order order) {
        String query = "DELETE FROM `order_has_products` WHERE orderID = ?";
        try {
            PreparedStatement preparedStatement = conn.prepareStatement(query);
            preparedStatement.setInt(1, order.getOrderId());
            preparedStatement.executeUpdate();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }

    }
}
