package tptema3.dao;

import com.sun.org.apache.xpath.internal.operations.Or;
import tptema3.model.Customer;
import tptema3.model.Order;
import tptema3.model.Product;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Alex Ichim on 09.04.2017.
 */
public class OrderDao {


    //JDBC driver name and db URL
    static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";
    static final String DB_URL = "jdbc:mysql://localhost:3306/warehouse";


    //Database credentials
    static final String USER = "root";
    static final String PASS = "";

    private Connection conn;

    public OrderDao(){
        try {
            Class.forName("com.mysql.jdbc.Driver");
            conn = DriverManager.getConnection(DB_URL, USER, PASS);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public int createOrder(Order order) {

        ProductOrderDao productOrderDao = new ProductOrderDao();
        String query = "INSERT INTO `order` (username, released) VALUES (?, ?)";
        int orderId = -1;
        try {
            PreparedStatement preparedStatement = conn.prepareStatement(query);
            preparedStatement.setString(1, order.getCustomer().getCustomerName());
            preparedStatement.setInt(2, 0);
            orderId = preparedStatement.executeUpdate();

            try {
                PreparedStatement preparedStatement1 = conn.prepareStatement("SELECT MAX(`orderId`) FROM `order`");
                ResultSet resultSet = preparedStatement1.executeQuery();
                if (resultSet.next()) {
                    orderId = resultSet.getInt(1);
                }
                for (Product prod: order.getProducts()
                     ) {
                    productOrderDao.create(prod, orderId);
                }

            } catch (SQLException e) {
                e.printStackTrace();
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return orderId;
    }

    public List<Order> getOrders() {
        ProductOrderDao productOrderDao = new ProductOrderDao();
        List<Order> orderList = new ArrayList<>();
        String query = "SELECT * FROM order";

        try {
            PreparedStatement preparedStatement = conn.prepareStatement(query);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                Order order = new Order();
                Customer customer = new Customer();
                customer.setCustomerName(resultSet.getString(2));
                order.setCustomer(customer);
                order.setProducts(productOrderDao.getProductsForOrder(resultSet.getInt(1)));
                orderList.add(order);
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return orderList;
    }

    public void releaseOrder(Order order) {
        String query = "UPDATE `order` SET released = ? WHERE orderId = ?";
        try {
            PreparedStatement preparedStatement = conn.prepareStatement(query);
            preparedStatement.setInt(1, 1);
            preparedStatement.setInt(2, order.getOrderId());
            preparedStatement.executeUpdate();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }

    public void deleteOrder(Order order) {
        String query = "DELETE FROM `order` WHERE orderID = ?";
        try {
            PreparedStatement preparedStatement = conn.prepareStatement(query);
            preparedStatement.setInt(1, order.getOrderId());
            preparedStatement.executeUpdate();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }
}
